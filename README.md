JeeWx 多触点管理平台，简称“捷微”.
===============

触点支持： 支持微信公众号、微信企业号、支付宝服务窗等触点
最新版本： 3.0.0（发布日期：20161220）
源码下载（迁移新址）：[http://git.oschina.net/jeecg/jeewx](http://git.oschina.net/jeecg/jeewx) 